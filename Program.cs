﻿using System;
using System.IO;
using System.Collections.Generic;
using DrawTools;

namespace PC_Builder
{
	class Profile
	{
		public string Name;
		public List<PC> Builds;

		public Profile()
		{
			Console.Write("Create username: ");
			this.Name = Console.ReadLine();
			Console.WriteLine("Hello, {0}!", this.Name);
		}

		public void CreateBuild()
		{
			PC build = new PC();
			this.Builds.Add(build);
		}
	}

	class PC
	{
		private string BuildName;
		public int Budget;
		public List<Hardware> Components;
		private string[] componentNames;

		public PC()
		{
			this.componentNames = new string[] {"random entry", "Processor", "Graphics Card", "Motherboard", "Memory", "Storage", "Power Supply"};
			Console.Write("Give a friendly name to your build: ");
			this.BuildName = Console.ReadLine();

			while (true)
			{
				Console.Write("What is your budget?: ");
				try
				{
					this.Budget = Convert.ToInt32(Console.ReadLine());
					break;
				} catch (FormatException)
				{
					Console.WriteLine("You may only enter numbers in this field!");
				}
			}

			//TODO: at this point, the user may interactively fill out each components' data in any order they wish
			ConsoleTools.Menu("Components", componentNames);
		}
	}

	class Hardware
	{
		public string Vendor, Name, Link;
		public int Price;
		public DateTime ProductionDate;
	}

	class CPU : Hardware
	{
	 	public float Frequency_Base, Frequency_Boost; //base and boost frequencies in MHz
		public int Cores, Threads; //number of processor cores and threads
		public string Cache; //amount of cache in MBs
		public int PSU_Minimum; //required minimum amount of power in Watts according to vendor
		public bool Unlocked; //is overclockable or not
	}

	class GPU : CPU
	{
		public string Interface; //PCI Express version
		public bool GSync, FreeSync, RayTracing; //support for gsync, freesync or raytracing technologies
		public Memory VRAM; //see Hardware.Memory class
		public List<string> Ports; //DisplayPort, VGA, HDMI, etc.
	}

	class Memory : Hardware
	{
		public int Size; //amount of memory in GBs
		public int Frequency; //clockspeed in MHz
		public string Type; //GDDR5, GDDR6, etc.
		public int Bandwidth; //memory bandwidth in GB/s-s
		public int Channels; //number of memory channels
	}

	class PSU : Hardware
	{
		public int MaxPower; //maximum power output in Watts
		public bool ShortCircuitProtection; //whether or not it has short circuit protection
		public int[] Dimensions; //3 integers: [0] as width; [1] as height; [2] as depth
	}

	class Storage : Hardware
	{
		public int Size; //size in GBs
		public string Connection; //M.2, SATA, etc.
		public int Read, Write; //read / write speeds in GB/s-s	
		public int[] Dimensions; //3 integers: [0] as width; [1] as height; [2] as depth
	}

	class HDD : Storage
	{
		public int RPM; //revolutions per minute (rpm)
	}
	
    	class Program
    	{
		static Profile prof;

        	static void Main(string[] args)
        	{
			PC pc = new PC();
        	}

		static void Init()
		{
			try
			{
				//load from save file
			} catch (Exception)
			{
				prof = new Profile();
			}
		}
	}
}

namespace DrawTools
{
	public static class ConsoleTools
	{
		public static void DrawBox(int left, int top, ConsoleColor color = ConsoleColor.White, char corner = '+', char lineH = '-', char lineV = '|') //draw a box with specific width (left), height (top), corner character (corner), horizontal line character (lineH) and vertical line character (lineV) and with the specified color (color) at the position of the screen where the cursor is at the time of calling this method
		{
			Console.ForegroundColor = color;
			WriteAt(corner, 0, 0);
			WriteAt(corner, left, 0);
			for (int i = 1; i <= left - 1; i++) //left - 1 because the last character will be a corner
			{
				WriteAt(lineH, i, 0);
				WriteAt(lineH, i, top);
			}
			WriteAt(corner, 0, top);
			WriteAt(corner, left, top);
			//drew top and bottom side
			
			for (int i = 1; i < top; i++)
			{
				WriteAt(lineV, 0, i);
				WriteAt(lineV, left, i);
			}
			//drew left and right side

			Console.ResetColor();
		}

		public static void WriteAt(char s, int x, int y) //writes specified character to a specified location in the console window
		{
			try
			{
				int origRow = Console.CursorTop;
				int origCol = Console.CursorLeft;

				Console.SetCursorPosition(origCol + x, origRow + y);
				Console.Write(s);

				Console.SetCursorPosition(origCol, origRow);
			} catch (ArgumentOutOfRangeException)
			{
				//ignore
			}
		}

		public static string Menu(string title, string[] entries, ConsoleColor titleColor = ConsoleColor.White, ConsoleColor borderColor = ConsoleColor.White) //returns string value of entry selected from an interactive menu bordered with specified color (optional)
		{
			//get longest entry from array to determine width of box
			int left = 0;
			int top = entries.Length + 5; //account for offset and title
			foreach (string s in entries)
			{
				if (s.Length > left)
				{
					left = s.Length;
				}
			}
			
			if (title.Length > left)
			{
				left = title.Length;
			}

			left += 4; //account for offset

			Console.Clear();

			//set the cursor in a position which will cause the box to be centered in the window
			int[] start = {Console.WindowWidth / 2 - (left / 2), Console.WindowHeight / 2 - (top / 2)};
			Console.SetCursorPosition(start[0], start[1]);

			ConsoleTools.DrawBox(left, top, borderColor);
			//draw title
			Console.SetCursorPosition((left/2 - (title.Length/2) + start[0]), start[1] + 2);
			Console.ForegroundColor = titleColor;
			Console.Write(title);
			Console.ResetColor();
			Console.SetCursorPosition(start[0] + 2, start[1] + 4); //offset from border of the box

			foreach (string s in entries)
			{
				Console.Write(s);
				Console.SetCursorPosition(Console.CursorLeft - s.Length, Console.CursorTop + 1);
			}

			//now the input part
			Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop - entries.Length); //return cursor to first menu entry
			top = start[1] + 4; //minimum Console.CursorTop value to stay on menu entries
			int bottom = start[1] + 3 + entries.Length; //maximum Console.CursorTop value to stay on menu entries, +3 because blank lines before and after title, and the title itself
			while (true)
			{
				switch (Console.ReadKey().Key)
				{
					case ConsoleKey.Enter:
						string result = entries[Console.CursorTop - (start[1] + 4)];
						Console.Clear();
						Console.WriteLine(result);
						return result;
						break;

					case ConsoleKey.UpArrow:
						Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop - 1);
						if (Console.CursorTop < top)
						{
							//reverse
							Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop + 1);
						}
						break;

					case ConsoleKey.DownArrow:
						Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop + 1);
						if (Console.CursorTop > bottom)
						{
							//reverse
							Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop - 1);
						}
						break;
				}
			}
		}
	}
}
